# Presenze PRO

<div align="center">

<img src="https://wyday.com/images/lm/langs/nodejs.electron.svg"  width="50%" height="300">

</div>


### Features ⭐
- Quick compilation of monthly attendance and allocations
- Auto start on login
- Integration with telegram bot to send the attendance sheet



### Requirements ⚙
- NodeJS & Npm
- Private settings file (./assets/settings/settings.json)


## Setup 🔍
Install all required dependencies using the command:
```
npm install
```

## Build 🛠
To build the deb and rpm packages simply type the command
```
npm run make
```
Folder "./out" will be generated with the packages inside

## Dev start 🐛
To start the software in develop mode

```
npm run start
```


