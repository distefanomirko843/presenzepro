const path = require('path')

module.exports = {
  packagerConfig: {
    icon:path.join(__dirname, '/assets/images/logo.png')
  },
  rebuildConfig: {},
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {},
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['darwin'],
    },
    {
      name: '@electron-forge/maker-deb',
      config: {
        icon:path.join(__dirname, '/assets/images/logo.png')
      },
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {
        icon:path.join(__dirname, '/assets/images/logo.png')
      },
    },
  ],
};
