const { ipcRenderer } = require('electron')
var _ = require('lodash');


(async () => {
    var developers = await ipcRenderer.invoke('synchronous-message', 'getDevelopers')

    var projects = await ipcRenderer.invoke('synchronous-message', 'getProjects')
    var savedConfig = await ipcRenderer.invoke('synchronous-message', 'getSaved')


    let developersList = Object.keys(developers)
    let projectList = Object.keys(projects)


    devSelect = document.getElementById('selectedDeveloper');
    projectSelect = document.getElementById('selectedProject');
    baseHours = document.getElementById('baseHours');
    extraHours = document.getElementById('extraHours');
    spinner = document.getElementById("fullscreenSpinner")
    selectedSlot = document.getElementById('selectedSlot');
    btnSend = document.getElementById('btnSend');
    btnSendTelegram = document.getElementById('btnSendTelegram');

    dateTimeLabel = document.getElementById('dateTime');
    saveConfig = document.getElementById('saveConfig');
    autoStart = document.getElementById('autoStart');

    var isAutoStartEnabled = await ipcRenderer.invoke('synchronous-message', "isAutostartEnabled");

    if (isAutoStartEnabled) {
        autoStart.checked = true;
    } else {
        btnSend
        autoStart.checked = false;
    }



    dateTimeLabel.innerHTML = new Date().toLocaleDateString('it-IT', { weekday: "long", year: "numeric", month: "long", day: "numeric" });


    btnSendTelegram.addEventListener("click", async (event) => {

        showSpinner();

        let objToSend = {
            type: "sendTelegram", data: {
                dev: devSelect.value,
                date: {
                    day: new Date().toLocaleDateString('it-IT', { day: "numeric" }),
                    year: new Date().toLocaleDateString('it-IT', { year: "numeric" }),
                    monthName: new Date().toLocaleDateString('it-IT', { month: "long" }),
                    monthNumber: new Date().toLocaleDateString('it-IT', { month: "numeric" }),
                }
            }
        };



        await ipcRenderer.invoke('synchronous-message', objToSend);

        hideSpinner();

    });

    projectSelect.addEventListener("change", (event) => {
        document.getElementById('selectedProjectDescription').value = projects[event.target.value].description;
    });



    btnSend.addEventListener("click", async (event) => {
        await showSpinner();
        let objToSend = {
            type: "send", data: {
                dev: devSelect.value,
                project: projectSelect.value,
                slot: selectedSlot.value,
                baseHours: baseHours.value,
                extraHours: extraHours.value,
                date: {
                    day: new Date().toLocaleDateString('it-IT', { day: "numeric" }),
                    year: new Date().toLocaleDateString('it-IT', { year: "numeric" }),
                    monthName: new Date().toLocaleDateString('it-IT', { month: "long" }),
                    monthNumber: new Date().toLocaleDateString('it-IT', { month: "numeric" }),
                }
            }
        };


        await ipcRenderer.invoke('synchronous-message', objToSend);
        if (saveConfig.checked) {

            let objToSave = _.cloneDeep(objToSend);

            objToSave.type = "save";

            await ipcRenderer.invoke('synchronous-message', objToSave);
        }


        await hideSpinner();

    });

    projectSelect.addEventListener("change", (event) => {
        document.getElementById('selectedProjectDescription').value = projects[event.target.value].description;
    });


    autoStart.addEventListener("change", async (event) => {

        showSpinner();


        if (autoStart.checked) {

            await ipcRenderer.invoke('synchronous-message', "enableAutoStart");

        } else {

            await ipcRenderer.invoke('synchronous-message', "disableAutoStart");

        }

        hideSpinner();
    });


    devSelect.addEventListener("change", (event) => {
        selectedSlot.innerHTML = "";
        selectedSlot.options[selectedSlot.options.length] = new Option("Mattina", developers[event.target.value].slot1);
        selectedSlot.options[selectedSlot.options.length] = new Option("Pomeriggio", developers[event.target.value].slot2);
        selectedSlot.options[selectedSlot.options.length] = new Option("Mat/Pom", developers[event.target.value].slot1 + ":" + developers[event.target.value].slot2);
        selectedSlot.dispatchEvent(new Event('change'))
    });






    for (let dev of developersList) {

        devSelect.options[devSelect.options.length] = new Option(dev, dev);
        devSelect.dispatchEvent(new Event('change'))

    }


    for (let project of projectList) {

        projectSelect.options[projectSelect.options.length] = new Option(project, project);

        projectSelect.dispatchEvent(new Event('change'))

    }


    if (savedConfig) {

        devSelect.value = savedConfig.dev;
        projectSelect.value = savedConfig.project;

        baseHours.value = savedConfig.baseHours;
        extraHours.value = savedConfig.extraHours;


        devSelect.dispatchEvent(new Event('change'))
        projectSelect.dispatchEvent(new Event('change'))

        selectedSlot.value = savedConfig.slot;

        selectedSlot.dispatchEvent(new Event('change'))

    }


    function hideSpinner() {
        spinner.classList.remove('d-flex')
        spinner.classList.add('d-none')
    }


    function showSpinner() {
        spinner.classList.remove('d-none')
        spinner.classList.add('d-flex')
    }

})();