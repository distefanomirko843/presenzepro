const { app, BrowserWindow } = require('electron')
const path = require('path')
const { GoogleSpreadsheet } = require('google-spreadsheet');
const { ipcMain } = require('electron') 
const fs = require('fs');
const ExcelJS = require('exceljs');
const ElectronGoogleOAuth2 = require('@getstation/electron-google-oauth2').default;
const process = require('process');
const checkInternetConnected = require('check-internet-connected');
const notifier = require('node-notifier');
const { appSettings } = require('./assets/settings/settings.json')
const AutoLaunch = require('easy-auto-launch');
const axios = require('axios');
const mime = require('mime-types');

const config = {
  timeout: 5000,
  retries: 3,
  domain: 'apple.com'
}


presenzePROAutoLauncher = new AutoLaunch({
  name: 'PresenzePRO',
  path: app.getPath('exe'),
});

arrDevelopers = {};
projects = {};

let folderPath = require('os').homedir() + "/presenzePRO";

if (!fs.existsSync(folderPath)) {

  fs.mkdirSync(folderPath);
  fs.mkdirSync(folderPath + "/fogli_presenze");

} else if (!fs.existsSync(folderPath + "/fogli_presenze")) {

  fs.mkdirSync(folderPath + "/fogli_presenze");

}



app.whenReady().then(async () => {

  await checkInternetConnected(config)
    .then(async () => {

      const googleDoc = new GoogleSpreadsheet(appSettings.spreadsheetId);
      const myApiOauth = new ElectronGoogleOAuth2(
        appSettings.googleOauthId,
        appSettings.googleOauthSecretId,
        appSettings.googleOauthScopes,
        { successRedirectURL: 'https://www.xacria.com/' },
        );


      await fs.access(path.join(folderPath + "/auth.json"), async (err) => {
        if (err) {


          await myApiOauth.openAuthWindowAndGetTokens()
            .then(token => {

              fs.writeFileSync(folderPath + "/auth.json", JSON.stringify(token));
              googleDoc.useOAuth2Client(myApiOauth.oauth2Client);

            });

        } else {

          const data = fs.readFileSync(folderPath + "/auth.json", { encoding: 'utf8', flag: 'r' });

          let dataJson = JSON.parse(data);

          await myApiOauth.setTokens({ refresh_token: dataJson.refresh_token });
          googleDoc.useOAuth2Client(myApiOauth.oauth2Client);

        }



        await inizialize(googleDoc);

        await createWindow();


        ipcMain.handle('synchronous-message', async (event, arg) => {

          switch (arg) {

            case 'getDevelopers': return arrDevelopers;
            case 'isAutostartEnabled': {
              return await presenzePROAutoLauncher.isEnabled();
            };
            case 'getProjects': return projects;
            case 'enableAutoStart': {
              await presenzePROAutoLauncher.enable();
              notifier.notify({
                title: 'Presenze PRO',
                message: 'Autostart abilitato!'
              });
              return null;

            }
            case 'disableAutoStart': {
              notifier.notify({
                title: 'Presenze PRO',
                message: 'Autostart disabilitato!'
              });
              await presenzePROAutoLauncher.disable();
              return null;

            }

            case 'getSaved': {

               let res = await  checkFileConfig(folderPath + "/config.json");

                return res;

            }; break;

          }




          if (typeof arg == 'object') {


            switch (arg.type) {

              case 'sendTelegram': {
                sendFileTelegram(arg)
               return null;
              }; break;

              case 'save': {
                fs.writeFileSync(folderPath + "/config.json", JSON.stringify(arg.data));
                return null;
              }; break;

              case 'send': {

                await updateGoogleSheets(googleDoc, arg);
                await updateLocalXlsx(googleDoc, arg)
                return null;

              }
            }


          }
        })


      })


    }).catch((err) => {
      notifier.notify({
        title: 'Presenze PRO',
        message: 'Connessione ad internet assente!'
      });

      process.exit();

    });


})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})



async function inizialize(googleDoc) {
  await googleDoc.loadInfo();

  let sheet = googleDoc.sheetsByTitle['LEGENDA'];


  await sheet.loadCells('C2:D40');


  for (let i = 2; i < 40; i++) {
    let cell = await sheet.getCellByA1('C' + i);
    if (cell.value) {
      let cellDescription = await sheet.getCellByA1('D' + i);

      projects[cell.value] = { code: cell.value, description: cellDescription.value }

    } else {
      break;
    }
  }


  let monthName = new Date().toLocaleDateString('it-IT', { month: "long" });
  let year = new Date().toLocaleDateString('it-IT', { year: "numeric" }) + "";
  monthName = monthName.toUpperCase();
  let sheetTitle = monthName + " " + year.substring(2, year.length);

  sheet = googleDoc.sheetsByTitle[sheetTitle];

  await sheet.loadCells('C5:C70');


  for (let i = 5; i < 70; i += 2) {
    const a1 = await sheet.getCellByA1('C' + i);
    if (!a1.value) break;


    arrDevelopers[a1.value] = { name: a1.value, slot1: i, slot2: i + 1 }
  }

}


async function checkFileConfig(filePath) {
  return new Promise((resolve, reject) => {
    fs.access(filePath, fs.constants.F_OK, (err) => {
      if (err) {
        resolve(null);
      } else {
       let res = fs.readFileSync(folderPath + "/config.json",
        { encoding: 'utf8', flag: 'r' });

        resolve(JSON.parse(res));
      }
    });
  });
}


async function updateLocalXlsx(googleDoc, arg) {
  try {

    let fileName = folderPath + "/fogli_presenze/Foglio_Presenze_" + arg.data.date.monthName + arg.data.date.year + "_" + arg.data.dev.replace(/ /g, "_") + ".xlsx"

    fs.access(path.join(fileName), (err) => {
      if (err) {

        fs.copyFile(app.getAppPath() + "/assets/fogliopresenze.xlsx", fileName, (err) => {
          if (err) {
            console.error('Errore durante la copia del file:', err);
          } else {
            console.log('File copiato con successo.');
            editOdsFile(arg, fileName);
          }
        });
      } else {
        console.log('Il file esiste già nella directory di destinazione.');
        editOdsFile(arg, fileName);
      }
    })


    notifier.notify({
      title: 'Presenze PRO',
      message: 'Presenze aggiornate!'
    });

  } catch (e) {
    notifier.notify({
      title: 'Presenze PRO',
      message: 'Errore aggiornamento presenze!'
    });
  }
}

async function updateGoogleSheets(googleDoc, arg) {

  try {

    let sheetTitle = arg.data.date.monthName.toUpperCase() + " " + arg.data.date.year.substring(2, arg.data.date.year.length);

    sheet = googleDoc.sheetsByTitle[sheetTitle];

    let arrCells = arg.data.slot.split(":");
    await sheet.loadCells('A1:AH60')

    for (let cell of arrCells) {
      let c = await sheet.getCell(+(cell + "") - 1, +(arg.data.date.day + "") + 2);
      c.value = arg.data.project;
      await sheet.saveUpdatedCells();
    }

    notifier.notify({
      title: 'Presenze PRO',
      message: 'Allocazioni aggiornate!'
    });

  } catch (e) {
    notifier.notify({
      title: 'Presenze PRO',
      message: 'Errore aggiornamento allocazioni!'
    });
  }
}

function createWindow() {

  const win = new BrowserWindow({
    width: 450,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    }
  })

  win.setResizable(false);
  win.setMenu(null);

  win.loadFile('index.html');

}

function editOdsFile(arg, file) {

  const workbook = new ExcelJS.Workbook();

  workbook.xlsx.readFile(file).then(() => {

    const worksheet = workbook.getWorksheet("Foglio1");
    const cell = worksheet.getCell('B6');
    cell.value = arg.data.dev;


    const cellDate = worksheet.getCell('C2');
    let month = (arg.data.date.monthNumber + "").length > 1 ? (arg.data.date.monthNumber + "") : '0' + (arg.data.date.monthNumber + "");
    cellDate.value = new Date(arg.data.date.year + "-" + month + "-01");
    cellDate.numFmt = 'yyyy-mm-dd';


    const cellDayBase = worksheet.getCell(6, +arg.data.date.day + 3);

    cellDayBase.value = arg.data.baseHours

    if (+arg.data.extraHours > 0) {
      const cellDayExtra = worksheet.getCell(7, +arg.data.date.day + 3);

      cellDayExtra.value = arg.data.extraHours

    }

    workbook.xlsx.writeFile(file);
  });
}


function sendFileTelegram(arg) {

  let fileSendName = "Foglio_Presenze_" + arg.data.date.monthName + arg.data.date.year + "_" + arg.data.dev.replace(/ /g, "_") + ".xlsx";
  let filePath = folderPath + "/fogli_presenze/Foglio_Presenze_" + arg.data.date.monthName + arg.data.date.year + "_" + arg.data.dev.replace(/ /g, "_") + ".xlsx"

  fs.access(filePath, (err) => {
    if (err) {

      notifier.notify({
        title: 'Presenze PRO',
        message: 'Non hai generato ancora nessun foglio presenze!'
      });
      
      
    } else {
     
  

  const fileContent = fs.readFileSync(filePath);

  const mimeType = mime.lookup(filePath);

  const fileBlob = new Blob([fileContent], { type: mimeType });
  const formData = new FormData();
  formData.append('chat_id', appSettings.telegramChannelID);
  formData.append('document', fileBlob, fileSendName);

  axios.post(`https://api.telegram.org/bot${appSettings.telegramBotToken}/sendDocument`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }).then(response => {
    notifier.notify({
      title: 'Presenze PRO',
      message: 'Foglio presenze inviato su telegram!'
    });

  }).catch(error => {
    notifier.notify({
      title: 'Presenze PRO',
      message: 'Errore nell\' invio del foglio presenze!'
    });
  });

}
})

}
